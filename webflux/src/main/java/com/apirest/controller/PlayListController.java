package com.apirest.controller;

import com.apirest.document.PlayList;
import com.apirest.service.PlayListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/playlist")
public class PlayListController {

    @Autowired
    private PlayListService service;

    @GetMapping
    public Flux<PlayList> findAll(){
      return  service.findAll();
    }

    @GetMapping("/{id}")
    public Mono<PlayList> findById(@PathVariable String id){
        return  service.findById(id);
    }

    @PostMapping
    public Mono<PlayList> save(@RequestBody PlayList playList){
        return  service.save(playList);
    }
}
