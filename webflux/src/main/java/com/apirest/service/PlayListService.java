package com.apirest.service;

import com.apirest.document.PlayList;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public interface PlayListService {

   Flux<PlayList> findAll();
   Mono<PlayList> findById(String id);
   Mono<PlayList> save(PlayList playList);

}
