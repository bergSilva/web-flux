package com.apirest;

import com.apirest.document.PlayList;
import com.apirest.repository.PlayListRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.UUID;
//
//@Component
//public class DummyData implements CommandLineRunner {
//
//    private  final PlayListRepository playListRepository;
//
//    public DummyData(PlayListRepository playListRepository) {
//        this.playListRepository = playListRepository;
//    }
//
//    @Override
//    public void run(String... args) throws Exception {
//        playListRepository.deleteAll()
//                .thenMany(
//                        Flux.just("API Rest ", "Deploy Aplicação", "Java 8")
//                        .map(nome -> new PlayList(UUID.randomUUID().toString(),nome))
//                        .flatMap(playListRepository::save))
//                .subscribe(System.out::println);
//    }
//}
